# @ipui/skin

this is just a skin to the project [ipfs-storage](https://gitlab.com/ipui/ipui-storage).

the idea is start to write apps that only have a little style just for layout purposes and let _sizes_ and _colors_ to be customizable with `--var` css variables.

maybe an image explains the point.

## without skin

without skin the app still shows itself as a functional but more important a "good looking" in quotes because you'll see below.

| state | preview |
| - | - |
| without skin at all, even without the base (normalization by [equalizer.css](https://www.npmjs.com/package/equalizer.css)) | ![without skin at all][without-skin-at-all] |
| only normalized | ![without skin 0][without-skin-0] |
| only normalized | ![without skin 1][without-skin-1] |

[without-skin-at-all]: ./assets/without-skin-at-all.png "without skin at all"
[without-skin-0]: ./assets/without-skin-0.png "without skin 0"
[without-skin-1]: ./assets/without-skin-1.png "without skin 1"

## actual themes

| name | preview |
| - | - |
| moon | ![moon][moon] |
| blood | ![blood][blood] |
| aqua | ![aqua][aqua] |

[moon]: ./assets/moon.png "moon"
[blood]: ./assets/blood.png "blood"
[aqua]: ./assets/aqua.png "aqua"

## last update
aug 25, 2019
