# packages
{ src, dest, parallel, watch, series } = require 'gulp'
babel        = require 'gulp-babel'
path         = require 'path'
autoprefixer = require 'gulp-autoprefixer'
concat       = require 'gulp-concat'
sass         = require 'gulp-sass'
sourcemaps   = require 'gulp-sourcemaps'
fs           = require 'fs-extra'

# environment variables
NODE_ENV = process.env.NODE_ENV || 'development'

{ dependencies, devDependencies } = config = require path.resolve __dirname, 'package.json'

PREFIX = './'

###
# build
###

# prepare
prepare = ->
  fs.ensureDir PREFIX

# js
js = ->

  task = src [ './src/**/*.js' ]
    .pipe sourcemaps.init()
    .pipe babel()
    .pipe sourcemaps.write('.')
    .pipe dest PREFIX

exports.js = js

# styles
styles = ->

  src [
    './src/core/index.sass'
    './src/shell/index.sass'
  ]
    .pipe sass().on('error', sass.logError)
    .pipe sourcemaps.init()
    .pipe autoprefixer()
    .pipe concat 'index.css'
    .pipe sourcemaps.write '.'
    .pipe dest PREFIX

exports.styles = styles

themes = ->

  src [
    './src/theme/*.sass'
    './src/theme/**/*.sass'
  ]
    .pipe sass().on( 'error', sass.logError )
    .pipe sourcemaps.init()
    .pipe autoprefixer()
    .pipe sourcemaps.write '.'
    .pipe dest path.join PREFIX, 'theme'

exports.themes = themes

# build
build = parallel js, styles, themes

exports.build = build

# watch
dev = ->

  watch [ './src/*.js', './src/**/*.js' ], js
  watch [ './src/*.sass', './src/**/*.sass' ], parallel styles, themes

exports.dev = series build, dev

exports.default = build

